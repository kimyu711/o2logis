<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-27
 * Time: 오후 7:09
 */

namespace O2pluss\O2logis\Handler;


interface ListHandler
{
    public function getList();
    public function setList();
    public function updateRow();
}