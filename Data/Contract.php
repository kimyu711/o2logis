<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    //
    protected $hidden = [
        'created_at', 'updated_at','id','human_id'
    ];
    public function human()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Human');
    }
}
