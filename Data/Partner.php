<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    //
    public function orders()
    {
        return $this->belongsToMany('O2pluss\O2logis\Data\Order');
    }
}
