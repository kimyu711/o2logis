<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Sido extends Model
{
    //
    protected $guarded = [];

    public function guguns()
    {
        return $this->hasMany('O2pluss\O2logis\Data\Gugun');
    }
}
