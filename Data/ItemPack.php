<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class ItemPack extends Model
{
    //
    public function route()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Route');
    }

    public function Item()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Item');
    }

}
