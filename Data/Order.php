<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $guarded=[];
    public function human()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Human');
    }

    public function routes()
    {
        return $this->hasMany('O2pluss\O2logis\Data\Route');
    }

    public function partners()
    {
        return $this->belongsToMany('O2pluss\O2logis\Data\Partner');
    }
}
