<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Dong extends Model
{
    //
    protected $guarded = [];

    public function addresses()
    {
        return $this->hasMany('O2pluss\O2logis\Data\Address');
    }

    public function gugun()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Gugun');
    }
}
