<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $guarded = [];
    public function dong()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Dong');
    }

    public function addressInfos()
    {
        return $this->hasMany('O2pluss\O2logis\Data\AddressInfo');
    }
}
