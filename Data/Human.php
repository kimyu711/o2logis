<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Human extends Model
{
    //
    protected $guarded=[];
    public function contracts()
    {
        return $this->hasMany('O2pluss\O2logis\Data\Contract');
    }

    public function addressInfo()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\AddressInfo');
    }

    public function orders()
    {
        return $this->hasMany('O2pluss\O2logis\Data\Order');
    }

    public function address()
    {
        return $this->hasOne('O2pluss\O2logis\Data\AddressInfo');
    }
}
