<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class AddressInfo extends Model
{
    //
    public function human()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Human');
    }

    public function address()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Address');
    }
}
