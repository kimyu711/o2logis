<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    //
    public function order()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Order');
    }

    public function addressInfo()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\AddressInfo');
    }

    public function itemPacks()
    {
        return $this->hasMany('O2pluss\O2logis\Data\ItemPack');
    }
}
