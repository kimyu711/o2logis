<?php

namespace O2pluss\O2logis\Command;

use Illuminate\Console\Command;

class LogisInitializing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'o2l:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        echo 'Declare Prefix for Class(Example... -Quick, Card, Etc... QuickOrder, CardOrder will be created) - ';
        $prefix=fgets(STDIN);
        echo $prefix.'Order';
    }
}
