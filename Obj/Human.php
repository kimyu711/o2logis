<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-27
 * Time: 오후 3:05
 */

namespace O2pluss\O2logis\Obj;


use O2pluss\O2logis\Data\Contract;

abstract class Human
{
    private $data;

    public function __construct($id)
    {
        $this->data=\O2pluss\O2logis\Data\Human::with('contracts','addressInfo')->find($id);
    }

    public function getContracts()
    {
        return $this->data->contracts;
    }

    public function setContract(Contract $contract)
    {
        return $this->data->contracts()->save($contract);
    }

    public function getName(){
        return $this->data->name;
    }

    public function setName($name)
    {
        $this->data->name=$name;
        return $this->data->save();
    }
}