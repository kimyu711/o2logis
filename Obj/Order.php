<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-27
 * Time: 오후 1:25
 */

namespace O2pluss\O2logis\Obj;

abstract class Order
{
    private $data;

    public function __construct($id)
    {
        $this->data=\O2pluss\O2logis\Data\Order::with('human.contract','human.address_info.address','routes.address_info.address','routes.address_info.human','routes.address_info.item_pack.items')->find($id);
    }

    public function getStart()
    {
        return $this->data->routes->first()->address_info;
    }

    public function getDest()
    {
        return $this->data->routes->last()->address_info;
    }

    public function getRoutes()
    {
        return $this->data->routes->pluck('address_info');
    }

    public function getItems()
    {
        return $this->data->routes->pluck('item_packs');
    }

    public function getData()
    {
        return $this->data;
    }

    abstract function getStatus();

    abstract function getOptions();

    abstract function setRoute();

    abstract function setItem();

    abstract function setStatus();

    abstract function setOptions();

    abstract function getPartnerList();

    abstract function getPrice();

    abstract function setPrice();
}