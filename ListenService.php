<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-28
 * Time: 오후 2:35
 */

namespace O2pluss\O2logis;


use O2pluss\O2logis\Handler\OrderHandler;
use O2pluss\O2logis\Handler\PartnerHandler;
use O2pluss\O2logis\Obj\Order;

class ListenService
{
    private $orderHandler;
    private $partnerHandler;
    public function __construct(OrderHandler $orderHandler, PartnerHandler $partnerHandler)
    {
        $this->orderHandler=$orderHandler;
        $this->partnerHandler=$partnerHandler;
    }

    public function getOrderList()
    {

    }

    public function updateOrder(Order $order)
    {
        $partnerList=$order->getPartnerList();
        $partnerList->each(function($partner){
           $partner->update();
        });
    }

    public function run()
    {
        $orderList=$this->getOrderList();
        $orderList->each(function($order){
           $this->updateOrder($order);
        });
    }
}